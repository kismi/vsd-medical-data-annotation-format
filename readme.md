#README

latest Version is 0.4.1

- changed the forms layout to reduce redundancy and simplify the file structure
- usergroups moved into the permission itemgroup
- remove the basic definitions

***please allow for drastic changes until version 1.0 is reached***

The MDAF is an adopted version of the CDISC-ODM v.1.3.0 standard. The MDAF supports annotation of these file types supported by the Virtualskeleton Database Framework

- Raw Images
- Segmented Images
- Clinical Trial data
- Statistical models
- Histology data

## File structure
### Repo <Repo>
The first part defines the input fields and groups available on the data repository. 

### RepoData <RepoData>
The actual data is stored here. The template contains example structured RepoData for the supported file types. Please store only one set of annotations per xml file. 

## Required data to adopt from the template in the Repo part
### MIAF 
- FileOID: Fill with an unique string which identifies the file
- CreationDateTime: adopt the timestamp

### Repo
- RepoName: use the ID of your repository
- RepoDescription: string to describe your repo
- RepoFormat: the fileformat you are using (eg RAW,SEG ... etc), should be set if you 

Two existing repo entries
		
	OID=REPO.CHIC
	<RepoName>CHIC Study</RepoName>
    <RepoDescription>CHIC Clinical Data Repo</RepoDescription>

    OID=REPO.SMIR
    <RepoName>SMIR</RepoName>
    <RepoDescription>SICAS Medical Image Repository</RepoDescription>
### Repo Data
Select the template to start with an empty value template of your choice

- RAW-template.xml (raw images (dicom etc.))
- SEG-template.xml (segmentation images)
- SM-template.xml (statistical models)
- CT-template.xml (clinical trial data)
- GEN-template.xml (genetic data)
- HISTO-template.xml (histology data)

#### Linking data
Use the ObjectID to link to an existing dataset

## To be defined
 - How to specify ontology annotations 
 - Import codelist directly from the Repo