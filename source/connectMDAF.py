# file: modules for working with the MIAF.xml

# Author Michael Kistler
# Version 0.1

import sys
import os
import inspect
import argparse
import datetime
import hashlib
try:
    import lxml.etree as ET
except:
    import xml.etree.ElementTree as ET



class  MDAFconnector:
    XMLFILE = os.path.join('template','RAW-template.xml')
    fileOid = str(hashlib.md5().hexdigest())
    NS = 'http://www.virtualskeleton.ch/ns/mdaf/v0.4.1'
    #repoDataOid = repoOid
    #subjectKey = fileOid

    def __init__(self,filename = None, ns = None):
        self.xmlfile = self.XMLFILE
        self.ns = self.NS
        if filename:
            self.xmlfile = filename 

        if  ns:
            self.ns = ns

        self.mdafns = {'mdaf':self.ns}
        print (self.xmlfile.exists())
        if self.xmlfile.exists():
            
            self.tree = ET.ElementTree()
            ET.register_namespace('ds','http://www.w3.org/2000/09/xmldsig#')
            ET.register_namespace('xsi','http://www.w3.org/2001/XMLSchema-instance')
            ET.register_namespace('xml','http://www.w3.org/XML/1998/namespace')
            ET.register_namespace('',self.ns)
            self.tree.parse(str(self.xmlfile))
            self.root = self.tree.getroot()

            if self.root.tag[0] == '{':
                self.uri, tag = self.root.tag[1:].split('}')
                self.nsUri = '{'+self.uri+'}'
            else:
                print ('namespace uri not available, exiting')
                sys.exit()
    
            self.repoTag = str(ET.QName(ns,'Repo'))
            self.globalVariablesTag = str(ET.QName(ns,'GlobalVariables'))
            self.metaDataVersionTag = str(ET.QName(ns,'MetaDataVersion'))
            self.dataFormatsTag = str(ET.QName(ns,'DataFormats'))
            self.dataFormatsRefTag = str(ET.QName(ns,'DataFormatsRef'))
            self.dataFormatsDefTag = str(ET.QName(ns,'DataFormatsDef'))
            self.formatRefTag = str(ET.QName(ns,'FormatRef'))
            self.formDefTag = str(ET.QName(ns,'FormDef'))
            self.itemGroupRefTag = str(ET.QName(ns,'ItemGroupRef'))
            self.itemGroupDefTag = str(ET.QName(ns,'ItemGroupDef'))
            self.itemRefTag = str(ET.QName(ns,'ItemRef'))
            self.itemDefTag = str(ET.QName(ns,'ItemDef'))
            self.codeListTag = str(ET.QName(ns,'CodeList'))
            self.codeListItemTag = str(ET.QName(ns,'CodeListItem'))
            self.descriptionTag = str(ET.QName(ns,'Description'))
            self.translateTextTag = str(ET.QName(ns,'TranslatedText'))
            self.questionTag = str(ET.QName(ns,'Question'))
            self.decodeTag = str(ET.QName(ns,'Decode'))

            self.repoPath = self.repoTag
            self.globalVariablesPath = self.repoTag+'/'+self.globalVariablesTag
            self.metaDataVersionPath = self.repoTag+'/'+self.metaDataVersionTag
            self.dataFormatsPath = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.dataFormatsTag
            self.dataFormatsRefPath = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.dataFormatsTag+'/'+self.dataFormatsRefTag
            self.dataFormatsDefPath = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.dataFormatsDefTag
            self.formatRefPath = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.dataFormatsDefTag +'/'+self.formatRefTag
            self.itemGroupDef = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.itemGroupDefTag
            self.itemDef = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.itemDefTag
            self.codeListPath = self.repoTag+'/'+self.metaDataVersionTag+'/'+self.codeListTag

            self.repoElement = self.root.find(self.repoPath)
            self.globalVariablesElement = self.root.find(self.globalVariablesPath)
            self.metaDataVersionElement =self.root.find(self.metaDataVersionPath)

            self.repoDataTag = str(ET.QName(ns,'RepoData'))
            self.objectDataTag = str(ET.QName(ns,'ObjectData'))
            self.dataFormatDataTag = str(ET.QName(ns,'DataFormatData'))
            self.formDataTag = str(ET.QName(ns,'FormData'))
            self.itemGroupDataTag = str(ET.QName(ns,'ItemGroupData'))
            self.itemDataTag = str(ET.QName(ns,'ItemData'))

            self.repoDataPath = self.repoDataTag
            self.objectDataPath = self.repoDataTag+'/'+self.objectDataTag
            self.dataFormatDataPath = self.repoDataTag+'/'+self.objectDataTag+'/'+self.dataFormatDataTag
            self.formDataPath =self.repoDataTag+'/'+self.objectDataTag+'/'+self.dataFormatDataTag+'/'+self.formDataTag
            self.itemGroupDataPath = self.repoDataTag+'/'+self.objectDataTag+'/'+self.dataFormatDataTag+'/'+self.formDataTag+'/'+self.itemGroupDataTag
            self.itemDataPath = self.repoDataTag+'/'+self.objectDataTag+'/'+self.dataFormatDataTag+'/'+self.formDataTag+'/'+self.itemGroupDataTag+'/'+self.itemDataTag

            self.repoDataElement = self.root.find(self.repoDataPath)
            self.objectDataElement = self.root.find(self.objectDataPath)
            self.dataFormatDataElement = self.root.find(self.dataFormatDataPath)


    def getTree(self):
        return self.tree

    def getRoot(self):
        return self.tree.getroot()

    def getRepoDataElement(self):
        return self.repoDataElement

    def addRepoDataElement(self, attributes):
        ''' desc '''
        self.repoDataElement = ET.SubElement(self.root, self.repoDataTag, attrib = attributes)
        return self.repoDataElement

    def addObjectDataElement(self, attributes):
        ''' desc '''
        self.objectDataElement = ET.SubElement(self.repoDataElement, self.objectDataTag, attrib = attributes)
        return self.objectDataElement

    def addDataFormatDataElement(self, attributes):
        self.dataFormatDataElement = ET.SubElement(self.objectDataElement, self.dataFormatDataTag, attrib = attributes)
        return self.dataFormatDataElement


    def addFormDataElement(self, attributes={}):
        if self.root.find(self.dataFormatDataPath).tag:
            self.formDataElement = ET.SubElement(self.root.find(self.dataFormatDataPath),
                self.formDataTag,
                attrib=attributes)
        else:
            self.formDataElement = None
        return self.formDataElement




    def getFormDataElements(self, oid = None):
        '''
        get a list of all form data elements

        :param oid: (str) identifier of the item data element
        :returns: a list of elements (list)
        '''
        if not oid:
            res = self.root.findall(self.formDataPath)
        else:
            res = self.tree.findall('.//{0}[@FormOID=\'FD.{1}\']'.format(self.formDataTag, oid),self.mdafns)

        return res



    def addItemGroupDataElement(self, element  =None, attributes = {}):
        if element == None:
            if self.root.find(self.formDataPath).tag:
                self.itemGroupDataElement =  ET.SubElement(self.root.find(self.formDataPath),
                    self.itemGroupDataTag,
                    attrib=attributes)
            else:
                self.itemGroupDataElement = None
        else:
            self.itemGroupDataElement = ET.SubElement(element,self.itemGroupDataTag,attrib=attributes)
        return self.itemGroupDataElement

    def getItemGroupDataElements(self, oid = None):
        '''
        find all ItemgGroupData entries with a specific OID

        :param oid: (str) identifier of the item data element
        :returns: a list of elements (list)
        '''
        if not oid:
            res = self.tree.findall(self.itemGroupDataPath)
        else:
            res = self.tree.findall('.//*{0}[@ItemGroupOID=\'IG.{1}\']'.format(self.itemGroupDataTag, oid))
        return res

    def getItemGroupDataElementByID(self,oid,ids):
        res = self.root.find('.//'+self.repoDataTag+'/'+self.objectDataTag+'/'+self.dataFormatDataTag+'/'+self.formDataTag+'[@FormOID=\'FD.{0}\']/*[@ID=\'{1}\']'.format(oid,ids))
        return res

    def addItemDataElement(self,element=None,attributes={}):
        if element == None:
            if self.root.find(self.itemGroupDataPath).tag:
                self.itemDataElement = ET.SubElement(self.root.find(self.itemGroupDataPath),
                    self.itemDataTag,
                    attrib=attributes)
            else:
                self.itemDataElement = None
        else:
            self.itemDataElement = ET.SubElement(element,self.itemDataTag,attrib=attributes)
        return self.itemDataElement

    def getItemDataElements(self, oid):
        '''
        find all ItemData entries with a specific OID 

        :param oid: (str) identifier of the item data element
        :returns: a list of elements (list)
        '''
        res = self.root.findall('.//{0}[@ItemOID=\'ID.{1}\']'.format(self.itemDataTag,oid))
        return res

    def loadXml(self):
        if os.path.isfile(self.xmlfile):
            print ('XML template file exists')
            self.tree = ET.ElementTree()
            ET.register_namespace('ds','http://www.w3.org/2000/09/xmldsig#')
            ET.register_namespace('xsi','http://www.w3.org/2001/XMLSchema-instance')
            ET.register_namespace('xml','http://www.w3.org/XML/1998/namespace')
            ET.register_namespace('','http://www.virtualskeleton.ch/ns/miaf/v0.4')
            self.dom = self.tree.parse(self.xmlfile)
            return self.dom
            print ('XML template file loaded')
        
        else:
            return None

    def saveXml(self,fp):
        fp = str(fp)
        self.tree.write(fp, encoding="UTF-8",xml_declaration=True, default_namespace="", short_empty_elements=True)
        print ('XML saved to ', fp)

    
    def getNsUri(self):
        self.nsUri = str(ET.QName(self.ns))
        return self.nsUri

    def setCreationDateTime(self, timestamp = datetime.datetime.now()):
        self.root.set('CreationDateTime',str(timestamp))

    def getCreationDateTime(self):
        return self.root.get('CreationDateTime')

    def setFileOID(self,fileOid=fileOid):
        self.root.set('FileOID',fileOid)

    def getFileOID(self):
        return self.root.get('FileOID')

    def getMDAFVersion(self):
        return self.root.get('MDAFVersion')
    
    def getfileType(self):
        return self.root.get('FileType')

    def setRepoOid(self,repoOid):
        self.repoOid = self.repoElement.set('OID',repoOid)

    def getRepoOid(self):
        return self.repoElement.get('OID')

    def setGlobalVariables(self,repoName,repoDesc,protocolName):
        self.globalVariablesElement.find(self.nsUri+'RepoName').text = repoName
        self.globalVariablesElement.find(self.nsUri+'RepoDescription').text = repoDesc
        self.globalVariablesElement.find(self.nsUri+'RepoFormat').text = protocolName
    
    def getGlobalVariables(self):
        return self.globalVariablesElement.find(self.nsUri+'RepoName').text,self.globalVariablesElement.find(self.nsUri+'RepoDescription').text, self.globalVariablesElement.find(self.nsUri+'RepoFormat').text 

    def setRepoData(self,repoDataOid):
        self.repoDataOid = self.repoDataElement.set('RepoOID',repoDataOid)

    def getRepoData(self):
        return self.repoDataElement.get('RepoOID')

    def getMetaDataVersion(self):
        return self.repoDataElement.get('MetaDataVersionOID')

    def setSubjectKey(self,subjectKey):
        if self.root.find('self.repoDataTag/{0}ObjectData'.format(self.nsUri)):
            self.subjectKey = self.root.find('self.repoDataTag/{0}ObjectData'.format(self.nsUri)).set('SubjectKey',subjectKey)
        else:
            self.subjectKey = None
        return self.subjectKey

    def getObjectData(self):
        return self.objectDataElement.get('SubjectKey')

    def setTag(self,elemTag):
        if self.ns:
            self.tag = str(ET.QName(self.ns,elemTag))
        else:
            print ('namespace uri not set, use setNsUri() to set')
            self.tag = None

    def getTag(self):
        if self.tag:
            return self.tag
        else:
            print ('tag not set, use setTag() to set')
            return None

    def getElement(self,elemRoot,elemUri):
        element = elemRoot.find(elemUri)
        if elem:
            return element
        else:
            return None

    def getAllElement(self,elemRoot,elemUri):

        elements = elemRoot.findall(elemUri)
        
        if len(elements) == 0:
            return None
        else: 
            return elements
    
    def setAttribute(self,element,attribute,value):
        element.set(attribute,value)

    def getAttribute(self,element,attribute):
        return element.get(attribute)

class MDAFreader(object):
    """
    used to read from MDAF xml files

    :param filenme: filepath of the xml file (Path)
    """
    def __init__(self,filename = None):
        self.xmlfile = filename 
        print (self.xmlfile.exists())
        
        if self.xmlfile.exists():
            self.tree = ET.ElementTree()
            self.tree.parse(str(self.xmlfile))
            self.root = self.tree.getroot()
            if self.root.tag[0] == '{':
                self.ns, tag = self.root.tag[1:].split('}')
                self.nsUri = '{'+self.ns+'}'
                self.mdafns = {'mdaf':self.ns}
                print ('namespace is:', self.ns)
            else:
                print ('namespace uri not available, exiting')
                sys.exit()

    def getTree(self):
        return self.tree
    
    def getRoot(self):
        return self.tree.getroot()

    def findFormData(self, OID):
        '''find a FormData Element'''
        return self.tree.find('.//mdaf:FormData[@FormOID=\'FD.{0}\']'.format(OID),self.mdafns)

    def findallItemGroupData(self, formDataElement):
        ''' 
        find all ItemGroupData element in a form element

        :param formDataElement: (ET element) formData element
        :returns: a list of ItemGroupData elements
        '''
        return formDataElement.findall('.//mdaf:ItemGroupData',self.mdafns)

    def findallItemData(self, itemGroupDataElement = None, oID = None):
        ''' 
        find all ItemData element in a form element or only specific if oID is given

        :param formDataElement: (ET element) itemGroupData element
        :param oID: (int) OID of the ItemData elements
        :returns: a list of ItemData elements
        '''
        if not itemGroupDataElement:
            itemGroupDataElement = self.tree
        
        search = None
        if oID:
            search = '[@ItemDataOID=\'ID.{0}\']'.format(oID)

        return itemGroupDataElement.findall('.//mdaf:ItemData{0}'.format(search),self.mdafns)


    def readPermission(self, objSelfUrl):
        '''
        reading the permission forms and creating appropriate permission sets 

        :param objSelfUrl: (str) the selfUrl of the related objectDataElement
        :returns: two list of data (json), one for the user, one for the groups 
        '''
        users = list()
        groups = list()
        form = self.findFormData('PERM')
        for itemgroup in self.findallItemGroupData(form):
            relatedObj = dict([('selfUrl', objSelfUrl)])
            rights = list()
            for item in self.findallItemData(itemgroup, oID = 'PERM'):
                rights.append(dict([('selfUrl', item.attrib['Value'])]))
            
            for item in self.findallItemData(itemgroup, oID = 'USER'):
                data = dict()
                data['relatedObject'] = relatedObj
                data['relatedUser'] = dict([('selfUrl', item.attrib['Value'])])
                data['relatedRights'] = rights
                users.append(data)
                
            for item in self.findallItemData(itemgroup, oID = 'GROUP'):
                data = dict()
                data['relatedObject'] = relatedObj
                data['relatedGroup'] = dict([('selfUrl', item.attrib['Value'])])
                data['relatedRights'] = rights
                groups.append(data)
        return users, groups

    def readLicense(self):
        '''
        reading license form and creating the license data

        :param objSelfUrl: (str) the selfUrl of the related objectDataElement
        :returns: a list of license dictionaries or a single dictionary
        '''
        element = self.findallItemData(oID = 'LICENSE')
        if len(element) == 1:
            licenses = dict([('selfUrl',element[0].get('Value'))])
        elif len(element) > 1:
            licenses = list()
            for e in element:
                licenses.append(dict([('selfUrl',e.get('Value'))]))
        else:
            licenses = None

        return licenses

    def readFiles(self):
        ''' 
        reading list of file from the file FormData element
        :returns: list of filenames
        '''
        files = list()
        element = self.findallItemData(oID = 'FILE')
        for e in element:
            files.append(e.get('Value'))

        return files

    def readModality(self):
        '''
        reading modality form and creating the license data

        :param objSelfUrl: (str) the selfUrl of the related objectDataElement
        :returns: a list of modality
        '''
        element = self.findallItemData(oID = 'MOD')
        if len(element) == 1:
            mods = dict([('selfUrl',element[0].get('Value'))])
        elif len(element) > 1:
            mods = list()
            for e in element:
                mods.append(dict([('selfUrl',e.get('Value'))]))
        else:
            mods = None

        return mods

    def readDescription(self):
        '''
        reading description form and creating the description 

        :param objSelfUrl: (str) the selfUrl of the related objectDataElement
        :returns: a list of description
        '''
        element = self.findallItemData(oID = 'DESC')
        if len(element) == 1:
            return element[0].get('Value')
        elif len(element) > 1:
            descs = list()
            for e in element:
                descs.append(e.get('Value'))
        else:
            return  None


