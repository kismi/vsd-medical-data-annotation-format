# file: modules for creating the template .xml

# Author Michael Kistler
# Version 0.1

import sys
import os
import inspect
import argparse
import datetime
import hashlib
import xml.etree.cElementTree as ET
from xml.dom import minidom
from pathlib import Path, PurePath, WindowsPath

import connectVSD

def prettify(elem):
    '''
    Return a pretty-printed XML string for the Element.
    
    :param elem: (ET.element) xml element
    :returns: pretty printed string for the xml tree
    '''
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")

def saveXml(root,fp,xmlns):
    '''
    wraps the root into a tree, registers the namespace and writes it to a file using the filename

    :param root: (ET.element) root element object
    :param xmlns: (str) the namespace of the xml file
    :param fp: desired filepath to save the xml file
    '''
    tree = ET.ElementTree(root)
    ET.register_namespace('ds','http://www.w3.org/2000/09/xmldsig#')
    ET.register_namespace('xsi','http://www.w3.org/2001/XMLSchema-instance')
    ET.register_namespace('xml','http://www.w3.org/XML/1998/namespace')
    ET.register_namespace('',xmlns)
    fp = str(fp)
    tree.write(fp, encoding="UTF-8",xml_declaration=True, default_namespace="", short_empty_elements=True)
    print ('XML saved to ', fp)

def loadXml(fp,xmlns):
    if fp.is_file():
        print ('XML template file exists')
        tree = ET.ElementTree()
        ET.register_namespace('ds','http://www.w3.org/2000/09/xmldsig#')
        ET.register_namespace('xsi','http://www.w3.org/2001/XMLSchema-instance')
        ET.register_namespace('xml','http://www.w3.org/XML/1998/namespace')
        ET.register_namespace('',xmlns)
        dom = tree.parse(str(fp))
        return dom
        print ('XML template file loaded')
    
    else:
        return None

def elementAttrib(element,attributes):
    '''
    description:
    creates attributes of an element based on the attributes dict
    parameters:
    element = element object
    attributes = dict with attribute name and attribute value
    return:
    '''
    for a, v in attributes.items():
        element.set(a,v)
    
def subElementAttrib(element,subElementName,attributes=None):
    '''
    description:
    creates a subelement its attributes based on the attributes dict
    parameters:
    element = element object
    subElementName = element to be created 
    attributes = dict with attribute name and attribute value
    return:
    the created SubElement
    '''
    
    subelem = ET.SubElement(element,subElementName)
    if attributes != None:
        for a, v in attributes.items():
            subelem.set(a,v)
    return subelem

def subElementText(element,subElementName,texts):
    '''
    description:
    creates a subelement and children in it based on the texts dict
    parameters:
    element = element objects
    subElementName = element to be created 
    texts = dict of child name and child text value
    return:
    the created SubElement
    '''
    
    subelem = ET.SubElement(element,subElementName)
    for t, v in texts.items():
        e = ET.SubElement(subelem,t)
        e.text = v
    return subelem


def addComment(elem,text):
    '''
    Description: append a comment to an element
    Parameters:
    elem = element object
    text = string to be appended as comment
    '''
    comment = ET.Comment(text)
    elem.append(comment)

def keyList(dictionary):
    ''' 
    Description: transform dictionary keys into a list
    Parameters: 
    dictionary: python dictionary
    return: list of dictionary keys, sorted
    '''
    klist = []
    for k in sorted(dictionary.keys()):
        klist.append(k)
    return klist

def indexedDict(keyList, valueList):
    '''
    Description: creates an indexed dictionary (eg a dictionary holding as value another dictionary object) from two codeLists
    Parameters
    keyList: list of key value for the dictionary
    valueLIst: list of values for the dictionary
    return:
    success: dictionary object 
    failure: None
    '''
    itemDict = dict()
    if len(keyList) == len(valueList):
        for i in range (len(valueList)):
            itemDict[i] = dict([(keyList[i],valueList[i])])
        return itemDict
    else:
        return None

templateList = [1,2,3,4,5,9]
#templateList = [1]

for template in templateList:
    
    api=connectVSD.VSDConnecter()
    
    ## templates 
    dataFormatsDict = dict([
    ('1','RAW'), #implemented
    ('2','SEG'), #implemented
    ('3','CT'), #implemented
    ('4','SM'), #implemented
    ('5','GEN'), #implemented
    ('6','MA'),
    ('7','LM'),
    ('8','SURF'),
    ('9', 'HISTO') #implemented
    ])

    ### Global Data 
    mdafVersion = '0.4.1'


    ### Repo Data


    repoOid = 'SMIR.REPO'

    repoName = 'Sicas medical image repository'     
    repoDesc = 'SMIR description of the process'
    repoFormat = dataFormatsDict[str(template)] ## RAW

    metaDataVersionName = 'Meta Data Version '+mdafVersion
    metaDataVersionOid = 'MD.'+mdafVersion


    raw = 'RAW Image'
    seg = 'Segmentation Image'
    ct = 'Clinical Trial'
    sm = 'Statistical Model'
    gen = 'Gene Expression'
    ma = 'Micro Array'
    lm = 'Landmarks'
    surf = 'Surface Model'
    histo = 'Histology'



    ## available data formats list

    dataFormatsNameDict = dict([
        ('RAW', raw),
        ('SEG', seg),
        ('CT', ct),
        ('SM', sm),
        ('GEN', gen),
        ('MA', ma),
        ('LM', lm),
        ('SURF', surf),
        ('HISTO', histo)
        ])


    ## standard generic data forms dicts
    itemDataFILE = dict([
        ('FILE','File list')
        ])

    itemDataLICENSE = dict([
        ('LICENSE','License')
        ])

    itemDataLINK = dict([
        ('LINK','Related data')
        ])

    itemDataPERM = dict([
        ('PERM','Permissions'),
        ('USER','User Information'),
        ('GROUP','Group Information')
        ])

    ## filetype dicts
    itemDataRAW = dict([
        ('DESC','Description'),
        ('MOD','Modality'),
        ('ANATOMY','Anatomical Region'),
        ('AGE','Age'),
        ('AGEUNIT','Age Unit'),
        ('SEX','Gender'),
        ('DOMINANCE','Dominance'),
        ('HEIGHT','Height (m)'),
        ('WEIGHT','Weight (kg)'),
        ('SLICE','Slice Thickness (mm)'),
        ('SPACE','Slice Spacing (mm)'),
        ('PKV','Peak Kilo Voltage')
        ])

    itemDataSEG = dict([
        ('DESC','Description'),
        ('MOD','Modality'),
        ('ANATOMY','Anatomical Region'),
        ('SEGMETHOD','Segmentation Method'),
        ('SEGMETHODDESC','Segmentation Method Description'),
        ])

    itemDataSM = dict([
        ('ANATOMY','Anatomical Region'),
        ('DESC','Description')
        ])

    itemDataGEN = dict([
        ('DESC','Description'),
        ])

    itemDataCT = dict([
        ('DESC','Description'),
        ])


    itemDataHISTO = dict([
        ('DESC', 'Description'),
        ('ANATOMY', 'Anatomical Region'),
        ('AGE', 'Age'),
        ('AGEUNIT', 'Age Unit'),
        ('SEX', 'Gender'),
        ('HISTOLOGY', 'Histology'),
        ('GRADING', 'Grading'),
        ('TUMORSIZE', 'Tumor size'),
        ('TUMORSIZEUNIT', 'Tumor size unit'),
        ('TCLASSIFICATION', 'T Classification'),
        ('NCLASSIFICATION', 'N Classification'),
        ('STAGING', 'Staging'),
        ('STAGINGLEVEL', 'Staging level'),
        ('PROLIFERATIONRATE', 'Proliferation rate'),
        ('STAINING', 'Staining Name'),
        ('STAININGVALUE', 'Staining Value'),
        ('MVD', 'Microvessel density')
        ])

    ## combine all itemData dics of the different formats into one
    itemDataDef =dict([
        ('RAW', itemDataRAW),
        ('SEG', itemDataSEG),
        ('SM', itemDataSM),
        ('GEN', itemDataGEN),
        ('CT', itemDataCT),
        ('HISTO', itemDataHISTO)
        ])

    ## For Data form defs, used to create the itemData at the end.
    formDataDef = dict([
        ('OI','Object Information'),
        ('FILE','File(s)'),
        ('LINK','Related Data Information'),
        ('LICENSE','License Information'),
        ('PERM','File Permissions')
        ])


    ## set the dataFormat FD.OI to the selected filetype
    for v,a in sorted(dataFormatsDict.items()):
        if a == repoFormat:
            itemDataOI = itemDataDef[a] 

    ## set the list of FormData
    formDataItemDef = dict([
        ('FILE',itemDataFILE),
        ('LICENSE',itemDataLICENSE),
        ('LINK',itemDataLINK),
        ('PERM',itemDataPERM),
        ('OI',itemDataOI)
        ])

    formDataItemDefList = keyList(formDataItemDef)


    ## Add CodeList elements
    codeLists= dict([
        ('GENDER','text'),
        ('RACE','text'),
        ('PREDICATE','text')
        ])


    codeListGender = dict([
        ('1','Male'),
        ('2','Female'),
        ('3','Other')
        ])

    codeListRace = dict([
        ('1','Caucasian'),
        ('2','African'),
        ('3','Asian'),
        ('4','Other')
        ])

    codeListPredicate = dict([('has_anatomical_region','http://www.virtualskeleton.ch/pred/has_anatomical_region'),
        ('has_modality','http://www.virtualskeleton.ch/pred/has_anatomical_region')])

    res = api.getRequest('object_rights')

    codeListPermission = dict()

    for key in res.get('items'):
        name = key['name']
        value = key['id']
        codeListPermission[str(value)] = str(name)

    codeListPermission = dict(
        [('1','None'),
        ('2','Visit'),
        ('3','Read'),
        ('4','Download'),
        ('5','Edit'),
        ('6','Manage'),
        ('7','Owner')
        ])

    res = api.getRequest('licenses')

    codeListLicense = dict()
    for key in res.get('items'):
        name = key['name']
        value = key['id']
        codeListLicense[str(value)] = str(name)


    mods = api.getModalityList()
    codeListModality = dict()
    for mod in mods:
        codeListModality[str(mod.id)] = str(mod.name)


    codeListDefs = dict([
        ('GENDER',codeListGender),
        ('RACE',codeListRace),
        ('PREDICATE',codeListPredicate),
        ('PERM',codeListPermission),
        ('LICENSE',codeListLicense),
        ('MOD', codeListModality)
        ])

###
    fileOut = Path('templates', repoFormat + '-template')
    fileOid = str(hashlib.md5().hexdigest())
    fileType = 'Annotation'
    creationDateTime = datetime.datetime.now()
    xmlNs = 'http://www.virtualskeleton.ch/ns/mdaf/v'+mdafVersion

    rootAttributes = dict([
        ('xmlns',xmlNs),
        ('MDAFVersion',mdafVersion),
        ('FileOID',fileOid),
        ('FileType',fileType),
        ('CreationDateTime',str(creationDateTime))
        ])


#################################################
## start the xml building
#################################################

    ## Set Root element and its attributes
    tree = ET.ElementTree()
    ET.register_namespace('ds','http://www.w3.org/2000/09/xmldsig#')
    ET.register_namespace('xsi','http://www.w3.org/2001/XMLSchema-instance')
    ET.register_namespace('xml','http://www.w3.org/XML/1998/namespace')
    ET.register_namespace('','http://www.virtualskeleton.ch/ns/mdaf/v' + mdafVersion)
    root = ET.Element('MDAF')
    elementAttrib(root,rootAttributes)

    ## Set the Repo element and its attributes
    repoAttrib = dict([
        ('OID',repoOid)
        ])
    repo = subElementAttrib(root,'Repo',repoAttrib)

    ## Add the GlobalVariables element
    globalVariablesTexts = dict([
        ('RepoName',repoName),
        ('RepoDescription',repoDesc),
        ('RepoFormat',repoFormat)
        ])
    globalVariables = subElementText(repo,'GlobalVariables',globalVariablesTexts)

    ## Add the MetaDataVersion element and its attributes
    metaVersionAttrib = dict([
        ('OID',metaDataVersionOid),
        ('Name', metaDataVersionName)
        ])
    metaDataVersion = subElementAttrib(repo,'MetaDataVersion',metaVersionAttrib)

    ## Add DataFormats Element and attributes (subelement of the MetaDataVersion element )
    dataFormats = subElementAttrib(metaDataVersion,'DataFormats')
    dataFormatRefAttrib = dict([
        ('ObjectFormatOID','DF.REPO')
        ])

    ## Add DataFormatRef element to the DataFormats element
    dataFormatRef = subElementAttrib(dataFormats,'DataFormatsRef',dataFormatRefAttrib)
    dataFormatDefAttrib = dict([
        ('OID','DF.REPO'),
        ('Name','REPO')
        ])

    ## Add DataFormatDef element and attributes as subelement of the MetaDataVersion element
    dataFormatDef = subElementAttrib(metaDataVersion,'DataFormatDef',dataFormatDefAttrib)
    for a,v in sorted(formDataDef.items()):
        elem = ET.SubElement(dataFormatDef,'FormRef')
        elem.set('FormOID','FD.'+a)

    ## Add the FormDef element to the MetaDataVersion element
    for a,v in sorted(formDataDef.items()):
        elem = ET.SubElement(metaDataVersion,'FormDef')
        elem.set('OID','FD.'+a)
        elem.set('Name',v)
        subelem = ET.SubElement(elem,'ItemGroupRef')
        subelem.set('ItemGroupOID','IG.'+a)

    ## Add the ItemGroupDefs as subelement of MetaDataVersion
    for a,v in sorted(formDataDef.items()):
        elem = ET.SubElement(metaDataVersion,'ItemGroupDef')
        elem.set('OID','IG.'+a)
        elem.set('Name',v)
        descElem = ET.SubElement(elem,'Description')
        textElem = ET.SubElement(descElem,'TranslatedText')
        textElem.text = v
        textElem.set('xml:lang','en')
        for att, value in sorted(formDataItemDef[a].items()):
            itemElem = ET.SubElement(elem,'ItemRef')
            itemElem.set('ItemOID','ID.'+att)

    ## Add all ItemDef elements
    for a,v in sorted(formDataItemDef.items()):
        for att,value in sorted(v.items()):
            ## ItemDefs
            #createItemDef(metaDataVersion,att,value)
            itemDefElem = ET.SubElement(metaDataVersion,'ItemDef')
            itemDefElem.set('OID','ID.'+att)
            itemDefElem.set('Name',value)
            itemDefElem.set('DataType','text')
            itemDescElem = ET.SubElement(itemDefElem,'Description')
            itemDescText = ET.SubElement(itemDescElem,'TranslatedText')
            itemDescText.set('xml:lang','en')
            itemDescText.text = value
            itemQuestionElem = ET.SubElement(itemDefElem,'Question')
            itemQuestionElem.set('xml:lang','en')
            itemQuestionText = ET.SubElement(itemQuestionElem,'TranslatedText')
            itemQuestionText.set('xml:lang','en')
            itemQuestionText.text = value


    for att, value in sorted(codeListDefs.items()):
        codeListElem = ET.SubElement(metaDataVersion,'CodeList')
        codeListElem.set('OID','CL.'+att)
        codeListElem.set('Name',att)
        codeListElem.set('DataType','text')
        for v,a in sorted(value.items()):
            elem = ET.SubElement(codeListElem,'CodeListItem')
            elem.set('CodedValue',v)
            decodeElem = ET.SubElement(elem,'Decode')
            textElem = ET.SubElement(decodeElem,'TranslatedText')
            textElem.set('xml:lang','en')
            textElem.text = a


    ## Set repoData for the selected filetype
    repoData = ET.SubElement(root,'RepoData',dict([('RepoOID',repoOid)]))
    objectData = ET.SubElement(repoData,'ObjectData')
    objectData.set('SubjectKey',fileOid)
    dataFormatData = ET.SubElement(objectData,'DataFormatData')
    dataFormatData.set('DataFormatOID','DF.REPO')


    for ind,dicts in sorted(formDataItemDef.items()):

        valueList = ['',]*len(keyList(dicts))
        itemDataDict = indexedDict(valueList,keyList(dicts))

        formData = ET.SubElement(dataFormatData,'FormData')
        formData.set('FormOID','FD.'+ind)
        itemGroupData = ET.SubElement(formData,'ItemGroupData')
        itemGroupData.set('ItemGroupOID','IG.'+ind)
        itemGroupData.set('ID',str(0))

        for k,v in sorted(itemDataDict.items()):
            elem = ET.SubElement(itemGroupData,'ItemData')
            for key,value in v.items():
                elem.set('ItemOID','ID.'+value)
                elem.set('Value',str(key))
                elem.set('ID',str(0))


    ## create nice output
    fileOut = Path(fileOut.with_suffix('.xml'))

    saveXml(root,fileOut,xmlNs)

    ## prettify the output with indents (4)
    xmltree = loadXml(fileOut,'http://www.virtualskeleton.ch/ns/mdaf/v' + mdafVersion)
    rootstr = prettify(xmltree)
    with fileOut.open('w') as file_:
        file_.write(rootstr)

